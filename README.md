# Vhdl coding

In this project, we will be building several hardware structures (adders, multipliers, dividers, multiplexers etc) on RTL level using VHDL and optimizing them using pipelining, parallelization, termsharing, retiming etc.
